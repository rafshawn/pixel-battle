package com.shawnnibras.battle.system;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.shawnnibras.battle.PixelBattle;
import com.shawnnibras.battle.screens.StartScreen;

public class WorldController extends InputAdapter {
    private PixelBattle game;
    private static final String TAG = WorldController.class.getName();

    public WorldController() {
        init();
    }

    private void return2Start() {
        game.setScreen(new StartScreen(game));
    }

    private void init() {
        Gdx.input.setInputProcessor(this);
        // initTestObjects;
    }

    public void update(float deltaTime) {

    }

    @Override
    public boolean keyUp (int keycode) {
        if (keycode == Keys.ESCAPE) {
           init();
        }
        return false;
    }
}