package com.shawnnibras.battle.system;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SoundController {
    boolean enabled = true;
    Music music;
    Sound sfx;
    float fxVolume = 0.5f;
}
