package com.shawnnibras.battle.scenes;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.*;
import com.shawnnibras.battle.PixelBattle;

import java.awt.*;

public class HUD implements Disposable {
    public Stage stage;
    private Viewport viewport;

    private Integer worldTimer;
    private float timeCount;
    private Integer gokuHealth;
    private Integer piccoloHealth;

    Label countdownLabel;
    Label timeLabel;
    Label levelLabel;
    Label gokuLabel;
    Label gokuHealthLabel;
    Label piccoloLabel;
    Label piccoloHealthLabel;

    public HUD(SpriteBatch sb) {
        worldTimer = 60;
        timeCount = 0;
        gokuHealth = 100;
        piccoloHealth = 100;

        viewport = new FitViewport(PixelBattle.V_WIDTH, PixelBattle.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, sb);

        Table table = new Table();
        table.top();
        table.setFillParent(true);

        countdownLabel = new Label(String.format("%02d", worldTimer), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        gokuHealthLabel = new Label(String.format("%03d", gokuHealth), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        piccoloHealthLabel = new Label(String.format("%03d", piccoloHealth), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        timeLabel = new Label("TIME", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        levelLabel = new Label("Battle Stage", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        gokuLabel = new Label("GOKU", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        piccoloLabel = new Label("PICCOLO", new Label.LabelStyle(new BitmapFont(), Color.WHITE));

        table.add(gokuLabel).expandX().padTop(10);
        table.add(timeLabel).expandX().padTop(10);
        table.add(piccoloLabel).expandX().padTop(10);
        table.row();
        table.add(gokuHealthLabel).expandX();
        table.add(countdownLabel).expandX();
        table.add(piccoloHealthLabel).expandX();

        stage.addActor(table);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
