package com.shawnnibras.battle.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class GameOptions {
    // Options Variables
    private static final String OPTIONS_NAME = "gameOptions";
    private static final String MUSIC_ENABLE = "music.enabled";
    private static final String MUSIC_VOLUME = "music.volume";
    private static final String SOUND_ENABLE = "sound.enabled";
    private static final String SOUND_VOLUME = "sound.volume";

    protected Preferences getPrefs() {
        return Gdx.app.getPreferences(OPTIONS_NAME);
    }

    public boolean isMusicEnabled() {
        return getPrefs().getBoolean(MUSIC_ENABLE, true);
    }

    public void setMusicEnabled(boolean musicEnabled) {
        getPrefs().putBoolean(MUSIC_ENABLE, musicEnabled);
        getPrefs().flush();
    }

    public float getMusicVolume() {
        return getPrefs().getFloat(MUSIC_VOLUME, 0.5f);
    }

    public void setMusicVolume(float volume) {
        getPrefs().putFloat(MUSIC_VOLUME, volume);
        getPrefs().flush();
    }

    public boolean isSoundEffectsEnabled() {
        return getPrefs().getBoolean(SOUND_ENABLE, true);
    }

    public void setSoundEffectsEnabled(boolean soundEffectsEnabled) {
        getPrefs().putBoolean(SOUND_ENABLE, soundEffectsEnabled);
        getPrefs().flush();
    }

    public float getSoundVolume() {
        return getPrefs().getFloat(SOUND_VOLUME, 0.5f);
    }

    public void setSoundVolume(float volume) {
        getPrefs().putFloat(SOUND_VOLUME, volume);
        getPrefs().flush();
    }
}