package com.shawnnibras.battle.Gameplay;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.HashMap;

public class AnimationHandler {

    private float timer = 0;
    private boolean looping = true;
    private String current;
    private final HashMap<String, Animation<AtlasRegion>> animations = new HashMap<String, Animation<AtlasRegion>>();

    public void add(String name, Animation<AtlasRegion> animation){
        animations.put(name, animation);
    }

    public void setCurrent(String name){
        if (current == name) {
            return;
        }
        current = name;
        timer = 0;
        looping = true;
    }

    public void setCurrent(String name, boolean looping){
        setCurrent(name);
        this.looping = looping;
    }
    public void setAnimationDuration(long duration){
        animations.get(current).setFrameDuration(duration / ((float) animations.get(current).getKeyFrames().length * 1000));
    }

    public boolean isCurrent(String name){
        return current.equals(name);
    }
    public boolean isFinished(){
        return animations.get(current).isAnimationFinished(timer);
    }
    public int frameIndex(){
        return animations.get(current).getKeyFrameIndex(timer);
    }

    public TextureRegion getFrame(){
        timer += Gdx.graphics.getDeltaTime();
        return animations.get(current).getKeyFrame(timer, looping);
    }
}
