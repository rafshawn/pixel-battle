package com.shawnnibras.battle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.shawnnibras.battle.PixelBattle;

import static com.badlogic.gdx.graphics.Color.*;

public class StartScreen implements Screen {
    private PixelBattle game;
    private Stage uiStage;
    private Skin skin;
    private Music music;

    public StartScreen(PixelBattle pixelBattle) {
        game = pixelBattle;
        // Create stage and set as input processor
        uiStage = new Stage(new ScreenViewport());

        // Skins for UI
        skin = new Skin(Gdx.files.internal("terramotherui/terra-mother-ui.json"));

        // Music
        music = PixelBattle.manager.get("bgm/battleTheme.mp3", Music.class);
        music.setLooping(true);
        music.play();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(uiStage);
        // Creates table that fits screen with UI elements
        Table table = new Table();
        table.setFillParent(true);
        table.setDebug(false); // If true, drawDebug(ShapeRenderer) will be called for this actor.
        uiStage.addActor(table);

        // Create menu interface
        Label gameTitle = new Label("PIXEL BATTLE v0.1", skin, "giygas");
        TextButton startGame = new TextButton("Start Game", skin);
        TextButton options = new TextButton("Options", skin);
        TextButton exit = new TextButton("Exit Game", skin);

        // Title header format
        gameTitle.setFontScale(2);
        gameTitle.setColor(YELLOW);

        // Add game title and buttons to table
        table.add(gameTitle);
        table.row().pad(40, 0, 20, 0);
        table.add(startGame).fillX().uniformX();
        table.row();
        table.add(options).fillX().uniformX();
        table.row().pad(20, 0, 20, 0);
        table.add(exit).fillX().uniformX();
        table.row();

        // Button listeners
        // 1. Start Game
        startGame.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.changeScreen(PixelBattle.CONTROLS);
            }
        });

        // 2. Options
        options.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.changeScreen(PixelBattle.OPTIONS);
            }
        });

        // 3. Exit Game
        exit.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });
    }

    @Override
    public void render(float delta) {
        // Clears screen to make room for next set of images
        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Draws the stage
        uiStage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        uiStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        // To Fix: Buttons don't centre
        // uiStage.getViewport().update(PixelBattle.V_HEIGHT, PixelBattle.V_WIDTH, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        uiStage.dispose();
    }
}
