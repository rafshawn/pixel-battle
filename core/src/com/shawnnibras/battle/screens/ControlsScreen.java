package com.shawnnibras.battle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.shawnnibras.battle.PixelBattle;

public class ControlsScreen implements Screen {
    private PixelBattle game;
    private Stage uiStage;
    private Skin skin;

    public ControlsScreen(PixelBattle pixelBattle) {
        game = pixelBattle;
        uiStage = new Stage(new ScreenViewport());

        skin = new Skin(Gdx.files.internal("terramotherui/terra-mother-ui.json"));
    }

    @Override
    public void show() {
        uiStage.clear();
        Gdx.input.setInputProcessor(uiStage);

        // Create a table that fills the screen
        Table table = new Table();
        table.setFillParent(true);
        uiStage.addActor(table);

        // Controls labels
        Label player1Label = new Label( "PLAYER 1", skin, "giygas");
        Label p1Movement = new Label( "Movement - [ W ] [ A ] [ D ]", skin );
        Label p1Fight = new Label( "Fight - [ S ] [ C ] [ V ]", skin );
        Label player2Label = new Label( "PLAYER 2", skin, "giygas");
        Label p2Movement = new Label( "Movement - [ I ] [ J ] [ L ]", skin );
        Label p2Fight = new Label( "Fight - [ K ] [ . ] [ / ]", skin );
        Label continueLabel = new Label( "press enter to continue", skin, "giygas");

        table.add(player1Label).colspan(2);
        table.row().pad(10,0,0,10).colspan(2);
        table.add(p1Movement).center();
        table.row().pad(10,0,0,10).colspan(2);
        table.add(p1Fight).center();
        table.row().pad(10,0,0,10).colspan(2);
        table.add(player2Label).colspan(2);
        table.row().pad(10,0,0,10).colspan(2);
        table.add(p2Movement).center();
        table.row().pad(10,0,0,10).colspan(2);
        table.add(p2Fight).center();
        table.row().pad(10,0,0,10).colspan(2);
        table.add(continueLabel).colspan(2);
    }

    private void handleInput(float dt) {
        if(Gdx.input.isKeyPressed(Input.Keys.ENTER)) {
            game.changeScreen(PixelBattle.FIGHT);
        }
    }

    public void update(float dt) {
        handleInput(dt); // Return to Start
    }

    @Override
    public void render(float delta) {
        update(delta);

        Gdx.gl.glClearColor(0f, 0f, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // tell our stage to do actions and draw itself
        uiStage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        uiStage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
