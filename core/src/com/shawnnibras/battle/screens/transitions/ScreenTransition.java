package com.shawnnibras.battle.screens.transitions;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

// Query duration of transition effect and let
// render effects for current and next screens.
public interface ScreenTransition {
    public float getDuration();
    public void render(SpriteBatch batch, Texture currScreen, Texture nextScreen, float alpha);
}
