package com.shawnnibras.battle.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.viewport.*;
import com.shawnnibras.battle.PixelBattle;
import com.shawnnibras.battle.scenes.HUD;
import com.shawnnibras.battle.sprites.Goku;
import com.shawnnibras.battle.sprites.Piccolo;
import com.shawnnibras.battle.system.WorldController;

public class FightScreen extends BaseScreen implements Screen {
    private PixelBattle game;
    private TextureAtlas atlas;
    private WorldController worldController;

    private boolean paused;

    // Battle Screen variables
    private OrthographicCamera gamecam; // Note: In WorldRenderer class... Should it still be here?
    private Viewport gamePort;
    private HUD hud;

    // Map Loader and variables
    private TmxMapLoader mapLoader;
    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;

    // Box2D variables
    private World world;
    private Box2DDebugRenderer b2dr;

    // Character sprites
    private Goku player1;
    private Piccolo player2;

    // Player Jumps
    private int player1Jumps,player2Jumps;


    public FightScreen(PixelBattle game){
        atlas = new TextureAtlas("sprites/fighters/fighter.atlas");

        this.game = game;
        gamecam = new OrthographicCamera();

        // Maintain virtual aspect ratio regardless of screen size
        gamePort = new FitViewport(PixelBattle.V_WIDTH / PixelBattle.PPM, PixelBattle.V_HEIGHT / PixelBattle.PPM, gamecam);

        // Create FightScreen HUD
        hud = new HUD(game.batch);

        // Load map and setup map renderer
        mapLoader = new TmxMapLoader();
        map = mapLoader.load("arenas/tournament.tmx");
        renderer = new OrthogonalTiledMapRenderer(map, 1 / PixelBattle.PPM);

        // Camera start position at the centre of map
        gamecam.position.set(gamePort.getWorldWidth() - 135, (gamePort.getWorldHeight() / 2), 0);

        // Create world using Box2D Physics, where gravity at y-axis is -10
        world = new World(new Vector2(0, -12.5f), true);
        b2dr = new Box2DDebugRenderer();

        // Character Sprites
        player1 = new Goku(world, this); // Initialize Goku class object
        player2 = new Piccolo(world,this);

        player1Jumps = 1;
        player2Jumps = 1;


        BodyDef bdef = new BodyDef();
        PolygonShape shape = new PolygonShape();
        FixtureDef fdef = new FixtureDef();
        Body body;

        // Create ground bodies
        for(MapObject object : map.getLayers().get(2).getObjects().getByType(RectangleMapObject.class)) {
            Rectangle rect = ((RectangleMapObject) object).getRectangle();

            bdef.type = BodyDef.BodyType.StaticBody;
            bdef.position.set((rect.getX() + rect.getWidth() / 2) / PixelBattle.PPM, (rect.getY() + rect.getHeight() / 2) / PixelBattle.PPM);

            body = world.createBody(bdef);

            shape.setAsBox(rect.getWidth() / 2 / PixelBattle.PPM, rect.getHeight() / 2 / PixelBattle.PPM);
            fdef.shape = shape;
            body.createFixture(fdef);
        }

        // Render character
        game.batch.setProjectionMatrix(gamecam.combined);
    }

    public TextureAtlas getAtlas() {
        return atlas;
    }

    @Override
    public void show() {

    }

    private void handleInput(float dt) {
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            game.changeScreen(PixelBattle.START);
        }
    }

    private void handleInputplayer1(float dt, Piccolo enemy, FixtureDef AttackHitbox) {
        // Player 1 Keyboard Input

        if (Gdx.input.isKeyJustPressed(Input.Keys.W)) {
            // player1.b2body.applyLinearImpulse(new Vector2(0, 5.5f), player1.b2body.getWorldCenter(), true);
            player1.b2body.applyForceToCenter(0, 300f, true);
            player1.Jump();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A) && player1.b2body.getLinearVelocity().x >= -3) {
            //player1.b2body.applyLinearImpulse(new Vector2(-0.5f, 0), player1.b2body.getWorldCenter(), true);
            player1.b2body.applyForceToCenter(-30f, 0, true);
            player1.Left();
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D) && player1.b2body.getLinearVelocity().x <= 3) {
            //player1.b2body.applyLinearImpulse(new Vector2(0.5f, 0), player1.b2body.getWorldCenter(), true);
            player1.b2body.applyForceToCenter(30f, 0, true);
            player1.Right();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.C)){
            player1.Punch();
            // player2.AttackSensor.createFixture(AttackHitbox);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.V)){
            player1.Kick();
            // player2.b2body.createFixture(AttackHitbox);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.S)) {
            player1.Block();
            // player2.b2body.createFixture(AttackHitbox);
        }
    }

    private void handleInputplayer2(float dt, Goku enemy, FixtureDef AttackHitbox) {

        // Issue setting up the attack hitbox!
        /*
        CircleShape attackShape = new CircleShape();
        attackShape.setRadius(0.03f);

        attackShape.setPosition(player2.b2body.getPosition());

        AttackHitbox.shape = attackShape;

         */

        if (Gdx.input.isKeyJustPressed(Input.Keys.I)){
            //player1.b2body.applyLinearImpulse(new Vector2(0, 5.5f), player1.b2body.getWorldCenter(), true);
            player2.b2body.applyForceToCenter(0, 300f, true);
            player2.Jump();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.J) && player2.b2body.getLinearVelocity().x >= -3) {
            //player1.b2body.applyLinearImpulse(new Vector2(-0.5f, 0), player1.b2body.getWorldCenter(), true);
            player2.b2body.applyForceToCenter(-30f, 0, true);
            player2.Left();
        }
            //player1.b2body.setLinearVelocity(0,0);
        if(Gdx.input.isKeyPressed(Input.Keys.L) && player2.b2body.getLinearVelocity().x <= 3) {
            //player1.b2body.applyLinearImpulse(new Vector2(0.5f, 0), player1.b2body.getWorldCenter(), true);
            player2.b2body.applyForceToCenter(30f, 0, true);
            player2.Right();
        }
        if(Gdx.input.isKeyPressed(Input.Keys.PERIOD)){
            player2.Punch();
            // player2.AttackSensor.createFixture(AttackHitbox);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.SLASH)){
            player2.Kick();
            // player2.b2body.createFixture(AttackHitbox);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.K)) {
            player2.Block();
            // player2.b2body.createFixture(AttackHitbox);
        }

    }

    @Override
    public boolean keyDown(int keycode) {
        /*
        if(keycode == Input.Keys.ESCAPE){ // ESC button

        }
         */

        return super.keyDown(keycode);
    }

    public void update(float dt) {
        handleInput(dt); // Return to Start
        FixtureDef AttackHitboxdef = new FixtureDef();


        handleInputplayer1(dt, player2, AttackHitboxdef);
        handleInputplayer2(dt, player1, AttackHitboxdef);
        if(player2.AttackSensor.getFixtureList().notEmpty()){
            player2.AttackSensor.destroyFixture(player2.AttackSensor.getFixtureList().first());
        }

        // Affect how bodies react to collision
        world.step(1/60f, 6, 2);

        player1.update(dt, player2);
        player2.update(dt, player1);

        // Camera follows player 1 position
        gamecam.position.x = player1.b2body.getPosition().x;

        // Update gamecam coordinates
        gamecam.update();
        // Render only what the camera sees
        renderer.setView(gamecam);


    }

    @Override
    public void render(float delta) {
        update(delta);

        // Battle screen does not update if the game is paused
        /*
        if (!paused){
            worldController.update(delta);
        }
        */

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Render game map before HUD
        renderer.render();

        // Render Box2DDebugLines
        // b2dr.render(world, gamecam.combined);

        game.batch.setProjectionMatrix(gamecam.combined);
        game.batch.begin();
        player1.draw(delta,game.batch,player2);
        player2.draw(delta,game.batch, player1);
        game.batch.end();

        // Set batch to draw HUD Camera
        game.batch.setProjectionMatrix(hud.stage.getCamera().combined);
        hud.stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gamePort.update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }


    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
        world.dispose();
        b2dr.dispose();
        hud.dispose();
    }
}
