package com.shawnnibras.battle.sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.*;
import com.shawnnibras.battle.PixelBattle;
import com.shawnnibras.battle.screens.FightScreen;

public class Piccolo extends Character{


    public Piccolo(World world, FightScreen screen) {
        super(world, screen); // "goku" region of fighter.atlas
        this.world = world;
        define_start_position();

        //TextureAtlas charset = new TextureAtlas(screen.getAtlas().);

        Idle_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloIdle"));
        Walk_AnimationF = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloWalkF"));
        Walk_AnimationB = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloWalkB"));
        Punch_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloPunch"));
        Kick_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloKick"));
        Jump_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloJump"));
        Block_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("piccoloBlock"));
        setBounds(0,99999, (32 * 1.5f) / PixelBattle.PPM, (50 * 1.5f) / PixelBattle.PPM);
        currentFrame = Idle_Animation.getKeyFrame(elapsed_time,true);
        //setRegion(gokuIdle);
    }


    @Override
    public void define_start_position(){
        BodyDef bdef = new BodyDef();
        bdef.position.set(512 / PixelBattle.PPM, 64 / PixelBattle.PPM); // Start position
        definebdef(bdef);
    }




}
