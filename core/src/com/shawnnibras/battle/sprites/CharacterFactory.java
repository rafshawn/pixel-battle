package com.shawnnibras.battle.sprites;

import com.badlogic.gdx.physics.box2d.World;
import com.shawnnibras.battle.screens.FightScreen;

public class CharacterFactory {

    public Goku goku(World world, FightScreen screen) {
        return new Goku(world,screen);
    }

//    public Piccolo piccolo(World world, FightScreen screen){
//        return new Piccolo(world,screen);
//    }
}
