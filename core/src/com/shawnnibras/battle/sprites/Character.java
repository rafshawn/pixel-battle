package com.shawnnibras.battle.sprites;

import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.physics.box2d.*;
import com.shawnnibras.battle.PixelBattle;
import com.shawnnibras.battle.screens.FightScreen;
import sun.jvm.hotspot.opto.Block;

public abstract class Character extends Sprite {

    public World world;
    public Body b2body;
    public Body AttackSensor;
    protected Animation<TextureAtlas.AtlasRegion> Idle_Animation, Walk_AnimationF,Walk_AnimationB, Punch_Animation, Kick_Animation,Jump_Animation,Block_Animation;
    protected TextureRegion currentFrame;
    protected float frame_time = 1 / 3f;
    protected float elapsed_time;
    protected int Jump;

    public Character(World world, FightScreen screen) {

        Jump = 1;
    }

    public void update(float dt, Character enemy){
        setPosition(b2body.getPosition().x - getWidth() / 2, b2body.getPosition().y - getHeight() / 2);

        if(this.b2body.getPosition().x <= enemy.b2body.getPosition().x){
            if(currentFrame.isFlipX())
            currentFrame.flip(true,false);
        }
        else
            if(!currentFrame.isFlipX()){
                currentFrame.flip(true,false);
            }

    }

    public void draw(float delta, SpriteBatch batch,Character enemy){
        elapsed_time += delta;

        //TextureRegion currentFrame = Kick_Animation.getKeyFrame(elapsed_time,true);

        float frame_width = currentFrame.getRegionWidth() / 64f;
        float frame_height = currentFrame.getRegionHeight() / 64f;



        batch.draw(currentFrame,b2body.getPosition().x - frame_width/ 2f,b2body.getPosition().y - frame_height/4, frame_width, frame_height);
    }

    public void definebdef(BodyDef bdef){
        bdef.type = BodyDef.BodyType.DynamicBody;
        b2body = world.createBody(bdef);

        FixtureDef fdef = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(0.1f,0.2f);

        fdef.shape = shape;
        b2body.createFixture(fdef);

        AttackSensor = world.createBody(bdef);
    }

//    public void defineAttack(){
//        AttackSensor = world.createBody(new BodyDef());
//
//    }

    public abstract void define_start_position();

    public void Left(){
        currentFrame = Walk_AnimationB.getKeyFrame(elapsed_time,true);
    }

    public void Right(){
        currentFrame = Walk_AnimationF.getKeyFrame(elapsed_time,true);
    }

    public void Jump(){
        currentFrame = Jump_Animation.getKeyFrame(elapsed_time,true);
        this.Jump--;
    }

    public void Punch(){
        currentFrame = Punch_Animation.getKeyFrame(elapsed_time,true);
    }
    public void Kick(){
        currentFrame = Kick_Animation.getKeyFrame(elapsed_time,true);
    }
    public void Block(){
        currentFrame = Block_Animation.getKeyFrame(elapsed_time,true);
    }

    public void setJump(){
        this.Jump = 1;
    }

    public int getJump(){
        return this.Jump;
    }
}
