package com.shawnnibras.battle.sprites;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.physics.box2d.*;
import com.shawnnibras.battle.PixelBattle;
import com.shawnnibras.battle.screens.FightScreen;

public class Goku extends Character {

    public Goku(World world, FightScreen screen) {
        super(world, screen); // "goku" region of fighter.atlas
        this.world = world;
        define_start_position();

        //TextureAtlas charset = new TextureAtlas(screen.getAtlas().);

        Idle_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuIdle"));
        Walk_AnimationF = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuWalkF"));
        Walk_AnimationB = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuWalkB"));
        Punch_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuPunch"));
        Kick_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuKick"));
        Jump_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuJump"));
        Block_Animation = new Animation<TextureAtlas.AtlasRegion>(frame_time,screen.getAtlas().findRegions("gokuBlock"));
        setBounds(0,99999, (32 * 1.5f) / PixelBattle.PPM, (50 * 1.5f) / PixelBattle.PPM);
        currentFrame = Idle_Animation.getKeyFrame(elapsed_time,true);
        //setRegion(gokuIdle);

    }

    //public void update(float dt) {



//        if (b2body.getLinearVelocity().x == 0 && b2body.getLinearVelocity().y == 0){
//
//            currentFrame = Walk_AnimationB.getKeyFrame(elapsed_time,true);
//        }
        //System.out.println(b2body.getPosition());
        //else if(b2body.getPosition().y > )
            //TextureRegion currentFrame = Kick_Animation.getKeyFrame(elapsed_time,true);
       // }

    @Override
    public void define_start_position(){
        BodyDef bdef = new BodyDef();
        bdef.position.set(256 / PixelBattle.PPM, 64 / PixelBattle.PPM); // Start position
        definebdef(bdef);
    }

}
