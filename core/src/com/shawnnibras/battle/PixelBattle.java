package com.shawnnibras.battle;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.shawnnibras.battle.scenes.GameOptions;
import com.shawnnibras.battle.screens.*;
import com.shawnnibras.battle.system.WorldController;
import com.shawnnibras.battle.system.WorldRenderer;

public class PixelBattle extends Game {
	public static final int V_WIDTH = 500;
	public static final int V_HEIGHT = 300;
	public static final float PPM = 100; // Pixels per Meter

	private WorldController worldController;
	private WorldRenderer worldRenderer;

	public SpriteBatch batch; // Note: In WorldRenderer class... Should it still be here?
	private GameOptions preferences;

	public static AssetManager manager;

	private boolean paused;

	// Variables for different scenes in game
	private StartScreen startScreen;
	private OptionsScreen optionsScreen;
	private FightScreen fightScreen;
	private EndScreen endScreen;
	private ControlsScreen controlsScreen;

	// Scene order
	public static final int START = 0;
	public static final int OPTIONS = 1;
	public static final int FIGHT = 2;
	public static final int ENDGAME = 3;
	public static final int CONTROLS = 4;

	@Override
	public void create () {
		Gdx.app.setLogLevel(Application.LOG_DEBUG); // Print log to console during runtime

		// Initialize controller and renderer
		worldController = new WorldController();
		worldRenderer = new WorldRenderer(worldController);

		// Game is active on start
		paused = false;

		manager = new AssetManager();
		manager.load("bgm/battleTheme.mp3", Music.class);
		// manager.load("sfx/jumpSound.mp3", Sound.class);
		manager.finishLoading();

		setScreen(new StartScreen(this));
		preferences = new GameOptions();

		batch = new SpriteBatch();

		// this.setScreen(new FightScreen(this));
	}

	// Method to change between screen
	public void changeScreen(int screen) {
		switch (screen) {
			case START:
				if(startScreen == null)
					startScreen = new StartScreen(this);
				this.setScreen(startScreen);
				break;
			case OPTIONS:
				if(optionsScreen == null)
					optionsScreen = new OptionsScreen(this);
				this.setScreen(optionsScreen);
				break;
			case FIGHT:
				if(fightScreen == null)
					fightScreen = new FightScreen(this);
				this.setScreen(fightScreen);
				break;
			case ENDGAME:
				if(endScreen == null)
					endScreen = new EndScreen(this);
				this.setScreen(endScreen);
			case CONTROLS:
				if(controlsScreen == null)
					controlsScreen = new ControlsScreen(this);
				this.setScreen(controlsScreen);
		}
	}

	public GameOptions getPreferences() {
		return this.preferences;
	}

	@Override
	public void render () {
		// World not updated when game is paused
		if (!paused) {
			worldController.update(Gdx.graphics.getDeltaTime());
		}
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		// worldRenderer.render();
		super.render();
	}

	@Override
	public void resize (int width, int height) {
		worldRenderer.resize(width, height);
	}

	@Override
	public void pause() {
		paused = true;
	}

	@Override
	public void resume() {
		paused = false;
	}

	@Override
	public void dispose () {
		worldRenderer.dispose();
		batch.dispose();
	}
}
